package jp.alhinc.narita_takumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {

	public static void main(String[] args) {
		// コマンド引数が適当な数でない場合のエラー処理
		if((args == null) || (args.length != 1)) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		// 支店コード＝支店名,支店コード＝合計金額
		HashMap<String, String> nameMap = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();

		// 支店定義ファイルの読込
		if(!readDef(args[0], "branch.lst", "支店定義ファイル", "^[0-9]{3},.+$", nameMap, salesMap)) {
			return;
		}

		// 売上ファイルの読込
		if(!readSales(args[0], salesMap)) {
			return;
		}

		// 支店別集計ファイルの出力
		if(!filePrint(args[0], "branch.out", nameMap, salesMap)) {
			return;
		}

	}

	// 各定義ファイルを読み込むメソッド
	private static boolean readDef(String command, String openFile, String fileType, String fileFormat, HashMap<String, String> nameMap, HashMap<String, Long> salesMap) {
		String line;
		BufferedReader br = null;
		try {
			File file = new File(command, openFile);

			// 1-1 各ファイルが存在しない場合のエラー処理
			if(!file.exists()) {
				System.out.println(fileType + "が存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			while((line = br.readLine()) != null) {
				// 1-2 支店定義ファイルのフォーマットが不正の場合のエラー処理
				if(!line.matches(fileFormat)) {
					System.out.println(fileType + "のフォーマットが不正です");
					return false;
				}

				String[] str = line.split(",", -1);

				// 1-2 各ファイルのフォーマットが不正の場合のエラー処理
				if(str.length != 2){
					System.out.println(fileType + "のフォーマットが不正です");
					return false;
				}

				nameMap.put(str[0], str[1]);
				salesMap.put(str[0], (long) 0);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	// 売上ファイルを読み込むメソッド
	private static boolean readSales(String command,HashMap<String, Long> salesMap) {
		// 売上ファイルの読込
		File file2 = new File(command);
		File[] list = file2.listFiles();

		// 条件に合うファイルの検索、抽出
		List<String> salesList = new ArrayList<String>();
		for(int i = 0; i < list.length; i++) {
			if(list[i].isFile()) {
				String filter = list[i].getName();
				if(filter.matches("^[0-9]{8}.rcd$")) {
					salesList.add(filter);
				}
			}
		}

		// 2-1 売上ファイルが連番になっていない場合のエラー処理
		List<String> checkList = new ArrayList<String>();
		for(int i = 0; i < salesList.size(); i++) {
			String check = salesList.get(i);
			String fileName = check.substring(0,check.lastIndexOf('.'));
			checkList.add(fileName);
		}

		for(int i = 0; i < (checkList.size() - 1); i++) {
			int checkNum1 = Integer.parseInt(checkList.get(i));
			int checkNum2 = Integer.parseInt(checkList.get(i + 1));
			if(checkNum1 != (checkNum2 - 1)) {
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}

		// 売上ファイル読込と金額の合計、保存
		String line2;
		BufferedReader br2 = null;
		for(int i = 0; i < salesList.size(); i++) {
			try {
				String salesFile = salesList.get(i);
				File file3 = new File(command, salesFile);
				FileReader fr3 = new FileReader(file3);
				br2 = new BufferedReader(fr3);

				List<String> contentList = new ArrayList<String>();

				while((line2 = br2.readLine()) != null) {
					contentList.add(line2);
				}

				// 2-4 売上ファイルの中身が2行でない場合のエラー処理
				if(contentList.size() != 2) {
					System.out.println(salesList.get(i) + "のフォーマットが不正です");
					return false;
				}

				// 2-3 該当する支店がない場合のエラー処理
				if(salesMap.get(contentList.get(0)) == null) {
					System.out.println(salesList.get(i) + "の支店コードが不正です");
					return false;
				}

				// 3-1 その他エラー
				if(!contentList.get(1).matches("^[0-9]{1,10}$")) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}

				long fileSales = Long.parseLong(contentList.get(1));
				long totalSales = salesMap.get(contentList.get(0)) + fileSales;

				// 2-2 合計金額が10桁を超えた場合のエラー処理(仮)
				if(String.valueOf(totalSales).length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return false;
				}

				salesMap.put(contentList.get(0), totalSales);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally {
				if(br2 != null) {
					try {
						br2.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
		}
		return true;
	}

	// 各ファイルの出力
	private static boolean filePrint(String command, String outputFile, HashMap<String, String> nameMap, HashMap<String, Long> salesMap) {
		BufferedWriter bw = null;
		try {
			File newfile = new File(command, outputFile);
			newfile.createNewFile();
			FileWriter fw = new FileWriter(newfile);
			bw = new BufferedWriter(fw);

			for(String key : nameMap.keySet()) {
				bw.write(key + "," + nameMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

}
